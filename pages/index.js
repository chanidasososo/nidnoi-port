import React from 'react';

export default class Index extends React.Component {
  
  render () {
    return (
      <>
        <header>
          <div className="container">
            <div className="row">
              <div className="col-6">
                <div className="port-name">
                  <span>Chanida</span>
                  <span>Klinsucon</span>
                </div>
                <div className="port-position">Frontend web developer</div>
                <div className="slogan">
                  I am enjoy to work as a website developer and capable to achieve the target beyond company
                </div>
              </div>
              <div className="col-6">
                <img src="assets/images/port-pic.jpg" alt=""/>
              </div>
            </div>
          </div>
        </header>
      </>
    )
  }
}